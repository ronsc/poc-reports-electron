import { Injectable } from '@angular/core';
import { ElectronService } from 'ngx-electron';

@Injectable({
  providedIn: 'root'
})
export class PrinterService {
  constructor(private electron: ElectronService) {}

  listPrinters(): string[] {
    if (this.electron.isElectronApp) {
      const webContents = this.electron.remote.webContents.getFocusedWebContents();
      if (!webContents) {
        return [];
      }
      return webContents.getPrinters().map(printer => printer.name);
    }
    return [];
  }
}
