import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseApiUrl = 'http://localhost:8080/poc-reports/api/';

  constructor(private http: HttpClient) {}

  getPdfBuffer(endPoint: string) {
    return this.http.get(`${this.baseApiUrl}/${endPoint}`, {
      responseType: 'arraybuffer'
    });
  }

  postPdfBuffer(endPoint: string, body: any) {
    return this.http.post(`${this.baseApiUrl}/${endPoint}`, body, {
      responseType: 'arraybuffer'
    });
  }
}
