import { Injectable } from '@angular/core';
import { ElectronService } from 'ngx-electron';

@Injectable({
  providedIn: 'root'
})
export class PdfService {
  constructor(private electron: ElectronService) {}

  previewPdf(pdfBuff: ArrayBuffer) {
    try {
      const blob = new Blob([pdfBuff], {
        type: 'application/pdf'
      });
      const blobUrl = encodeURIComponent(URL.createObjectURL(blob));
      const viewerUrl = `assets/pdfjs/web/viewer.html?disableWorker=true&file=${blobUrl}`;
      const modal = window.open('', 'preview-pdf');
      modal.document.open();
      modal.document.write(`
          <!DOCTYPE html>
          <html>
            <head>
              <title>Preview PDF</title>
              <style>
                body,
                html {
                  width: 100%;
                  height: 100%;
                  overflow: hidden;
                  margin: 0;
                }
                iframe {
                  width: 100%;
                  height: 100%;
                  border: none;
                }
              </style>
            </head>
            <body>
              <iframe src="${viewerUrl}" allowfullscreen="" webkitallowfullscreen=""></iframe>
            </body>
          <html>
        `);
      modal.document.close();
    } catch (error) {
      console.error(error);
    }
  }

  printPdf(pdfBuff: ArrayBuffer, printerName = '') {
    return new Promise((resolve, reject) => {
      if (!this.electron.isElectronApp) {
        return reject('This Feature Not working with no Electron App');
      }

      this.electron.ipcRenderer.once('print-pdf-reply', (ev, error) =>
        !error ? resolve() : reject(error)
      );

      const pdfBase64 = btoa(
        String.fromCharCode.apply(null, new Uint8Array(pdfBuff))
      );

      this.electron.ipcRenderer.send('print-pdf', { pdfBase64, printerName });
    });
  }
}
