import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';
import { FormGroup, FormControl } from '@angular/forms';

import { take } from 'rxjs/operators';
import { PdfService } from './pdf.service';
import { PrinterService } from './printer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'poc-reports-electron';
  printers = [];
  formGroup = new FormGroup({
    printerName: new FormControl('')
  });

  constructor(
    private apiService: ApiService,
    private pdfService: PdfService,
    private printerService: PrinterService
  ) {}

  ngOnInit() {
    this.printers = this.printerService.listPrinters();
  }

  previewPdf() {
    this.apiService
      .getPdfBuffer('/report/test-fonts-fix')
      .pipe(take(1))
      .subscribe(pdfBuff => {
        this.pdfService.previewPdf(pdfBuff);
      });
  }

  printPdfDefaultPrinter() {
    this.apiService
      .getPdfBuffer('/report/test-fonts-fix')
      .pipe(take(1))
      .subscribe(pdfBuff => {
        this.pdfService.printPdf(pdfBuff);
      });
  }

  printPdfSelectPrinter() {
    const printerName = this.formGroup.get('printerName').value;
    if (printerName) {
      this.apiService
        .getPdfBuffer('/report/test-fonts-fix')
        .pipe(take(1))
        .subscribe(pdfBuff => {
          this.pdfService.printPdf(pdfBuff, printerName);
        });
    } else {
      alert('Please select printer name before.');
    }
  }
}
