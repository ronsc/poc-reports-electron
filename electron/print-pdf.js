const { ipcMain } = require('electron');

/**
 * Waiting for print-pdf event from renderer process.
 */
function PrintPdf() {
  const isDev = require('./is-dev')();

  ipcMain.on('print-pdf', (event, { pdfBase64, printerName }) => {
    const path = require('path');
    const tmp = require('tmp');
    const fs = require('fs');
    const { spawn } = require('child_process');
    let pdfTmp;

    try {
      pdfTmp = tmp.fileSync({
        prefix: 'tmp-report-',
        postfix: '.pdf',
        discardDescriptor: true
      });
      const pdfPath = pdfTmp.name;

      fs.writeFileSync(pdfPath, new Buffer(pdfBase64, 'base64'));
      const args = [pdfPath, printerName];
      const print = spawn('PDFtoPrinter.exe', args, {
        cwd: isDev
          ? path.join(__dirname, 'resources')
          : path.join(__dirname, '..', '..'),
        windowsHide: true
      });

      print.stderr.on('data', data => {
        console.log(`stderr: ${data}`);
        pdfTmp.removeCallback();
        event.sender.send('print-pdf-reply', error);
      });
      print.on('close', code => {
        console.log(`child process exited with code ${code}`);
        pdfTmp.removeCallback();
        event.sender.send('print-pdf-reply', null);
      });
    } catch (error) {
      console.log(error);
      if (pdfTmp) {
        pdfTmp.removeCallback();
      }
      event.sender.send('print-pdf-reply', error);
    }
  });
}

module.exports = PrintPdf;
