const electron = require('electron');
const { app, BrowserWindow } = electron;
const path = require('path');
const url = require('url');

const isDev = require('./is-dev')();
const printPdf = require('./print-pdf');
const newWindow = require('./new-window');

printPdf();

let win;

function createWindow() {
  /**
   * Get Current Main Display
   */
  const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize;

  win = new BrowserWindow({
    width,
    height,
    autoHideMenuBar: true,
    fullscreen: false,
    fullscreenable: true,
    show: false,
    resizable: true,
    webPreferences: {
      webSecurity: false,
      nativeWindowOpen: true
    }
  });

  win.maximize();
  if (isDev) {
    win.loadURL('http://localhost:4200');
    win.webContents.openDevTools();
  } else {
    win.loadURL(
      url.format({
        pathname: path.join(__dirname, 'dist/index.html'),
        protocol: 'file:',
        slashes: true
      })
    );
  }

  win.once('ready-to-show', () => {
    win.show();
  });

  win.on('closed', () => {
    win = null;
  });

  newWindow(win);
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});
