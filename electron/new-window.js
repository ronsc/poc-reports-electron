const { BrowserWindow } = require('electron');

/**
 * Open Print Window.
 */
function NewWindow(mainWindow) {
  mainWindow.webContents.on(
    'new-window',
    (event, url, frameName, disposition, options) => {
      if (frameName === 'preview-pdf') {
        event.preventDefault();
        Object.assign(options, {
          modal: true,
          parent: mainWindow,
          width: 800,
          height: 600
        });
        event.newGuest = new BrowserWindow(options);
      }
    }
  );
}

module.exports = NewWindow;
